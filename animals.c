///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Jonah Bobilin <jbobilin@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @28 January 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdbool.h>
#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
   switch (color){
   case BLACK:
      return("Black");
   case WHITE:
      return("White");
   case RED:
      return("Red");
   case BLUE:
      return("Blue");
   case GREEN:
      return("Green");
   case PINK:
      return("Pink");
   default:
      return("error, data not retrievable");
   }

   return NULL; // We should never get here
};

char* genderPrint (enum Gender gender){
   switch(gender){
   case MALE:
      return("Male");
   case FEMALE:
      return("Female");
   default:
      return("error, data not retrievable");
   }
}

char* catbreedsPrint (enum CatBreeds catbreeds){
   switch(catbreeds){
   case MAIN_COON:
      return("Main Coon");
   case MANX:
      return("Manx");
   case SHORTHAIR:
      return("Short Hair");
   case PERSIAN:
      return("Persian");
   case SPHYNX:
      return("Sphynx");
   default:
      return("error, data not retrievable");
   }
}

char* isfixedPrint (bool isFixed){
   if(isFixed == true)
      return("Yes");
   else if(isFixed == false)
      return("No");
   else
      return("error, data not retrievable");
}

