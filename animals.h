///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jonah Bobilin <@jbobilin@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   28 January 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#pragma once
/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
enum Gender {MALE, FEMALE};
char* genderPrint (enum Gender gender);

/// Return a string for the name of the color
/// @todo this is for you to implement
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};
char* colorName (enum Color color);

//Cat breeds
enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
char* catbreedsPrint (enum CatBreeds catbreeds);

char* isfixedPrint (bool isFixed);

